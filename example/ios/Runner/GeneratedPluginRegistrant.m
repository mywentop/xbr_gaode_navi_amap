//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<integration_test/IntegrationTestPlugin.h>)
#import <integration_test/IntegrationTestPlugin.h>
#else
@import integration_test;
#endif

#if __has_include(<xbr_gaode_navi_amap/XbrGaodeNaviAmapPlugin.h>)
#import <xbr_gaode_navi_amap/XbrGaodeNaviAmapPlugin.h>
#else
@import xbr_gaode_navi_amap;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [IntegrationTestPlugin registerWithRegistrar:[registry registrarForPlugin:@"IntegrationTestPlugin"]];
  [XbrGaodeNaviAmapPlugin registerWithRegistrar:[registry registrarForPlugin:@"XbrGaodeNaviAmapPlugin"]];
}

@end
